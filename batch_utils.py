def load_tag_db(csv_path):
    """
    Read a csv containing dicom tag IDs and tag names, and return a dictionary
    matching each tag name (key) to tag id (value)

    Args:
        csv_path:       A string or similar path to the csv file. Currently
                        requires columns to be set up in the same order as the
                        provided dicom_tags.csv

    Returns:
        tags:           A dictionary matching dicom tag names (string) to dicom
                        tag ID (a list of two strings)
    """
    tags = {}
    with open(csv_path, 'r') as f:
        # Skip first two lines
        next(f)
        next(f)
        for line in f:
            vals = line.split(',')
            key = vals[2]
            # The provided csv has double quotes in tag ID values that should be
            # removed
            tag_id = [vals[0].replace('"', ''), vals[1].replace('"', '')]
            tags[key] = tag_id
    return tags
