# Batchtribute

Change high-level dicom tags for DICOM files in a batch.

## How to use

Batchtribute doesn't require a lot of setup. Just make sure you have the
following installed and you should be good to go:

- [python](https://www.python.org/) >= 3.6
- [pydicom](https://github.com/pydicom/pydicom) >= 1.0

Edit the following constants in `change_dicom.py` as needed:

- `PATH`: set this to the folder containing the dicoms you wish to change. Note
  that this program searches `PATH` recursively so if your dicoms are in multiple
  sub-folders, that's perfectly ok.
  Of course, this is also a warning! This program **will** change **all** dicoms
  in any subfolders of `PATH`, so make sure you want all of them edited! As a
  safety feature, the original dicoms should be left untouched and instead new
  ones will be generated and stored in a `New_dicoms` folder inside of `PATH`,
  but you should still be cautious.

- `TAG_NAME`: the dicom tag whose value you wish to change.

- `NEW_TAG_VAL`: the new value to set to the dicom tag name.

- `DRY_RUN`: whether or not to only run a simulation showing what would have
  been changed without actually making changes. Set to `True` for a dry run.
  Defaults to `False`, i.e. changes will actually be made.

Now run `python3 change_dicom.py` and the dicom tag value should be changed in
all dicoms found.

You can review a log of all changes by reading the generated log named
`batch_changes_{date_timestamp}.txt` which will be saved in the specified
`PATH`. This is also helpful if you choose to do a dry run first before making
changes.

## License

This program is permissively licensed (MIT). See LICENSE.md for details.
